import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';


export default function Footer(props){
    return(
        <footer className="footer">
            <Box>
                <Container maxWidth="lg">
                    <Grid container spacing={5}>
                        <Grid item xs={12} sm={4}>
                            <Box borderBottom="1">
                              <p> You Clicked {props.text} times.</p>
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
           
        </footer>
    )
}